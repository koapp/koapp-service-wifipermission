# Documentation:

This plugin checks if the user has wifi enabled. If it is disabled ask to enable it

### Configuration:

This plugin is plug and play, so no prior configuration is necessary.
