(function () {
    angular
      .module('king.services.wifipermission', [])
      .run(loadFunction);
  
    loadFunction.$inject = ['configService'];
  
    function loadFunction(configService) {
      // Register upper level modules
      try {
        if (configService.services && configService.services.wifipermission) {
          askpermissionFunction(configService.services.wifipermission.scope);
        } else {
          throw "The service is not added to the application";
        }
      } catch (error) {
        console.error("Error", error);
      }
    }
  
    function askpermissionFunction(scopeData) {
        if (!cordova) return;
     
        function onError(error) {
            console.error("The following error occurred: " + error);
        }
  
        function evaluateAuthorizationStatus(status) {

            switch (status) {
            case 0:
                console.log("Wifi not avaliable");
                navigator.notification.confirm(
                "The wifi is off, do you want to switch it on?",
                function (i) {
                    if (i == 1) {
                        cordova.plugins.diagnostic.switchToWifiSettings();
                    }
                }, "Wifi not avaliable", ['Yes', 'No']);
                break;
                
            case 1:
                console.log("Wifi available");
                // Yay! use wifi
                break;
            }
        }
    
        function checkAuthorization() {
            cordova.plugins.diagnostic.isWifiEnabled(evaluateAuthorizationStatus, onError);
        }
    
        checkAuthorization();
    }
        // --- End servicenameController content ---
})();